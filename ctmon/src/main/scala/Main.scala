import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ ActorRef, ActorSystem, Behavior }

import scala.collection.JavaConverters._
import io.calidog.certstream.CertStream
import scala.io.StdIn
import com.datastax.oss.driver.api.core.CqlSession
import scala.util.Try
import com.datastax.oss.driver.api.core.config.DriverConfigLoader
import java.net.InetSocketAddress
import scala.util.Success
import scala.util.Failure
import ujson.read

final case class CTLog(log: String)
final case class InsertData(tld: String, sld: String, trd: String, domain: String, log: String)

object CTMonActor {
  private val client = ActorSystem(CassandraClient(), "cassandra-client")

  def apply(): Behavior[CTLog] = Behaviors.receive { (context, message) =>
    val parsedJson = read(message.log)
    val domains = parsedJson("data")("leaf_cert")("all_domains").arr.map(_.str).toList
    domains.foreach((domain: String) => {
      val filtered = domain.split("\\.").filterNot(_ == "*").reverse.map(_.toLowerCase)
      val parts = if (filtered.length == 2) filtered :+ "" else filtered
      val tld = parts.head
      val sld = parts.tail.head
      val trd = parts.tail.tail.head
      client ! InsertData(tld, sld, trd, domain, message.log)
    })
    Behaviors.same
  }

  def stop(): Unit = {
    client.terminate()
  }
}

@main def server() = {
  val system = ActorSystem(CTMonActor(), "ctmon")
  CertStream.onMessageString((message: String) => {
    system ! CTLog(message)
  })
  StdIn.readLine("Press ENTER to exit\n")
  system.terminate()
}

object CassandraClient {
  private val session: CqlSession = connect()

  def apply(): Behavior[InsertData] = Behaviors.receive { (context, message) =>
    val insert = session.prepare("INSERT INTO ctmon.ct_logs(tld, sld, trd, domain, log) VALUES (?, ?, ?, ?, ?)")
    Try {
      session.execute(insert.bind(message.tld, message.sld, message.trd, message.domain, message.log))
    } match {
      case Success(_) =>
        println("Data saved successfully")
      case Failure(e) =>
        println("An error occurred while saving data: " + e.getMessage)
        System.exit(1)
        null
    }
    Behaviors.same
  }

  def connect(): CqlSession = {
    val session: Try[CqlSession] = Try {
      val config: DriverConfigLoader = DriverConfigLoader.programmaticBuilder().build()
      CqlSession.builder()
      .withConfigLoader(config)
      .addContactPoint(new InetSocketAddress(sys.env("CASSANDRA_HOST"), 9042))
      .withLocalDatacenter(sys.env("CASSANDRA_CLUSTER_NAME"))
      .withKeyspace(sys.env("CASSANDRA_KEYSPACE"))
      .withCredentials(sys.env("CASSANDRA_USER"), sys.env("CASSANDRA_PASSWORD"))
      .build()
    }

    session match {
      case Success(s) => s
      case Failure(e) =>
        println("An error occurred while connecting to Cassandra: " + e.getMessage)
        System.exit(1)
        null
    }
  }
}