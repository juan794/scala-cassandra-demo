val scala3Version = "3.3.1"
val akkaVersion = "2.9.1"
val certStreamVersion = "0.3.3"
val driverVersion = "4.7.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "ctmonitor", 
    version := "0.1.0",

    assembly / assemblyJarName := "ctmonitor.jar",
    assembly / assemblyMergeStrategy := {
      case PathList("META-INF", _*) => MergeStrategy.discard
      case _                        => MergeStrategy.first
    },

    scalaVersion := scala3Version,

    libraryDependencies ++= Seq(
      "com.github.CaliDog" % "certstream-java" % certStreamVersion,
      "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
      "com.datastax.oss" % "java-driver-core" % "4.17.0",
      "com.lihaoyi" %% "upickle" % "3.1.3",
    ),

    resolvers ++= Seq(
      "jitpack" at "https://jitpack.io",
      "Akka library repository" at "https://repo.akka.io/maven"
    )
  )
